
// Creating reusable functions
let name = {
	firstName: 'Ash',
	lastName: 'Ketsum',
	age: 10,
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty'],
	},
	pokemon: ['eevee', 'vaporeon', 'flareon', 'glaceon'],
	introduce: function(){
		console.log(' Hello my name is ' + this.firstName + ' ' + this.lastName);
	}
}
name.introduce();
console.log(name.friends);
console.log(name.pokemon);
console.log('Vaporeon! I choose you!');

// Real world application of Objects
/* Scenario
	1. Create a game that would have several pokemon interact with each other
	2. Every pokemon would have the same set of stats, properties, and functions
*/

let myPokemon = {
	name: "Vaporeon",
	level: 10,
	health: 50,
	attack: 37,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
};
console.log(myPokemon);

// Pokemon Object Constructor
function Pokemon(name,level, health, attack){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log();
	};
	this.faint = function(){
		console.log(this.name + ' fainted. ');
	}
}

// Create new instances of the "Pokemon" object each with their unique properties
let vaporeon = new Pokemon("Vaporeon", 10, 50, 37);
let ekans = new Pokemon("Ekans", 8, 34, 11);

console.log(vaporeon);
vaporeon.tackle(ekans);
console.log(ekans);
ekans.faint(vaporeon);
//reduced health
//geodude painted
//pokemon

//
let meowth = new Pokemon('Meowth', 17, 10, 20);

vaporeon.tackle(meowth);
console.log(meowth);
meowth.faint(vaporeon);
